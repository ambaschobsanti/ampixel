//
//  Photo+Networking.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

// Frameworks
#import <AFNetworking/AFNetworking.h>

#import "Photo+Networking.h"

// Model
#import "PhotoMTL.h"

#import "AppDelegate.h"

static const NSString *kConsumerKey = @"cJQXjIYMN5XpvfPHjb1qyhaLFYCgIoLL9AAaQ7Pv";

@implementation Photo (Networking)

+ (void)requestRecentPhotoWithCompletionHandler:(void(^)())requestCompleteHandle {
    NSDictionary *param = @{
                            @"consumer_key": kConsumerKey,
                            @"image_size": @(4),
                            @"page": @(1),
                            @"feature": @"today"
                            };
    [self requestPhotoWithParam:param completionHandler:requestCompleteHandle];
}

+ (void)requestPhotoForCategoryName:(NSString *)categoryName completionHandler:(void(^)())requestCompleteHandle {
    NSDictionary *param = @{
                            @"consumer_key": kConsumerKey,
                            @"image_size": @(4),
                            @"page": @(1),
                            @"only": categoryName,
                            @"feature": @"today"
                            };
    [self requestPhotoWithParam:param completionHandler:requestCompleteHandle];
    
}

+ (void)requestPhotoWithParam:(NSDictionary *)param completionHandler:(void(^)())requestCompleteHandle {
    NSString *path = @"https://api.500px.com/v1/photos";
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    
    NSURLRequest *request = [manager.requestSerializer requestWithMethod:@"GET" URLString:path parameters:param error:nil];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, NSDictionary *responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSArray <NSDictionary *> *photosArray = responseObject[@"photos"];
            for (NSDictionary *photoDict in photosArray) {
                NSError *error;
                PhotoMTL *photoMTL = [MTLJSONAdapter modelOfClass:PhotoMTL.class fromJSONDictionary:photoDict error:&error];
                NSManagedObjectContext *managedObjectContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
                [MTLManagedObjectAdapter managedObjectFromModel:photoMTL
                                           insertingIntoContext:managedObjectContext
                                                          error:&error];
                
                if ([managedObjectContext save:&error]) {
                    if (requestCompleteHandle) {
                        requestCompleteHandle();
                    }
                } else {
                    // TODO: Handle
                }
            }
        }
    }];
    [dataTask resume];
}

@end
