//
//  AMCollectionViewDataBinding.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMCollectionViewDataBinding.h"

@interface AMCollectionViewDataBinding ()

@property (weak, nonatomic) UICollectionView *collectionView;

@end

@implementation AMCollectionViewDataBinding

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super init];
    if (self) {
        _collectionView = collectionView;
    }
    return self;
}

#pragma mark - DataBinding

- (void)reload {
    [self.collectionView reloadData];
}

@end
