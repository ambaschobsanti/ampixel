//
//  AMCategoryCell+ConfigureForCategory.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMCategoryCell.h"

@interface AMCategoryCell (ConfigureForCategory)

- (void)configureCellForCategory:(Category *)category;

@end
