//
//  AMCollectionViewDataBinding.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMDataBinding.h"

@interface AMCollectionViewDataBinding : NSObject <AMDataBinding>

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

@end
