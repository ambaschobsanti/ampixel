//
//  AMPreviewPhotoCell+ConfigureForPhoto.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMPreviewPhotoCell.h"

@interface AMPreviewPhotoCell (ConfigureForPhoto)

- (void)configureCellForPhoto:(Photo *)photo;

@end
