//
//  AMDataBinding.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMDataBinding <NSObject>

- (void)reload;

@end
