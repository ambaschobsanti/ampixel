//
//  AMPreviewPhotoCell+ConfigureForPhoto.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

// Frameworks
#import <SDWebImage/UIImageView+WebCache.h>

#import "AMPreviewPhotoCell+ConfigureForPhoto.h"

#import "Photo.h"

@implementation AMPreviewPhotoCell (ConfigureForPhoto)

- (void)configureCellForPhoto:(Photo *)photo {
    NSURL *photoURL = [NSURL URLWithString:photo.photoURLString];
    [self.previewImageView sd_setImageWithURL:photoURL placeholderImage:nil];
    self.titleLabel.text = photo.title;
    self.authorLabel.text = photo.author;
}

@end
