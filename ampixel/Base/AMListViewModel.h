//
//  AMListViewModel.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AMDataBinding.h"

@protocol AMListViewModel <NSObject>

- (NSInteger)numberOfSection;
- (NSInteger)numberOfRowForSection:(NSInteger)section;
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

- (instancetype)initWithObjectID:(NSNumber *)objectID dataBinding:(id<AMDataBinding>)dataBinding;

@end
