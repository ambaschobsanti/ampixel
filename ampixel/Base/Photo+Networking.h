//
//  Photo+Networking.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "Photo.h"

@interface Photo (Networking)

+ (void)requestRecentPhotoWithCompletionHandler:(void(^)())requestCompleteHandle;

+ (void)requestPhotoForCategoryName:(NSString *)categoryName completionHandler:(void(^)())requestCompleteHandle;

@end
