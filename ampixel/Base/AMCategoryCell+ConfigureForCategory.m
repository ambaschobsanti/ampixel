//
//  AMCategoryCell+ConfigureForCategory.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMCategoryCell+ConfigureForCategory.h"

#import "Category.h"

@implementation AMCategoryCell (ConfigureForCategory)

- (void)configureCellForCategory:(Category *)category {
    self.categoryLabel.text = category.categoryName;
}

@end
