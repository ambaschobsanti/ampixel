//
//  AMCategoryTableViewController.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMCategoryTableViewController.h"

// Views
#import "AMCategoryCell+ConfigureForCategory.h"

// Models
#import "AMCategoryViewModel.h"
#import "Category.h"
#import "Photo+Networking.h"

// ViewControllers
#import "AMPhotoPreviewCollectionViewController.h"

static NSString *AMCategoryCellIdentifier = @"CategoryCell";

@interface AMCategoryTableViewController ()

@property (strong, nonatomic) NSArray *category;
@property (strong, nonatomic) id<AMListViewModel> viewModel;

@end

@implementation AMCategoryTableViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    [Photo requestRecentPhotoWithCompletionHandler:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AMPhotoPreviewCollectionViewController *photoPreviewViewController = segue.destinationViewController;
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Category *category = [self.viewModel objectAtIndexPath:indexPath];

    photoPreviewViewController.navigationItem.title = category.categoryName;
    photoPreviewViewController.categoryID = category.categoryID;
}


- (void)configureView {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(reloadData)
                  forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Properties

- (id<AMListViewModel>)viewModel {
    if (!_viewModel) {
        _viewModel = [[AMCategoryViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.viewModel numberOfSection];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRowForSection:section];
}

#pragma mark - UITableView DataDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AMCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:AMCategoryCellIdentifier forIndexPath:indexPath];
    [cell configureCellForCategory:[self.viewModel objectAtIndexPath:indexPath]];
    
    return cell;
}

- (void)reloadData {
    __weak typeof(self) weakSelf = self;
    [Photo requestRecentPhotoWithCompletionHandler:^{
        [weakSelf.refreshControl endRefreshing];
    }];
}

@end
