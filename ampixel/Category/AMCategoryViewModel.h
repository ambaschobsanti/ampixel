//
//  AMCategoryViewModel.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMListViewModel.h"

@interface AMCategoryViewModel : NSObject <AMListViewModel>


@end
