//
//  AMCategoryCell.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  Category;

@interface AMCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end
