//
//  AMCategoryViewModel.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMCategoryViewModel.h"

#import "Category.h"

@interface AMCategoryViewModel ()

@property (strong, nonatomic) NSArray <Category *> *categories;

@end

@implementation AMCategoryViewModel

- (instancetype)initWithObjectID:(NSNumber *)objectID dataBinding:(id<AMDataBinding>)dataBinding {
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Properties

- (NSArray *)categories {
    if (!_categories) {
        _categories = [Category allCategories];
    }
    return _categories;
}

#pragma mark - Interface

- (NSInteger)numberOfSection {
    return 1;
}

- (NSInteger)numberOfRowForSection:(NSInteger)section {
    return self.categories.count;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    return self.categories[indexPath.row];
}

@end
