//
//  AMPhotoViewer.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "AMPhotoViewer.h"

@interface AMPhotoViewer ()

@property (nonatomic, readwrite, nullable) UIImage *image;
@property (nonatomic, readwrite, nullable) UIImage *placeholderImage;
@property (nonatomic, readwrite, nullable) NSData *imageData;
@property (nonatomic, readwrite, nullable) NSAttributedString *attributedCaptionTitle;
@property (nonatomic, readwrite, nullable) NSAttributedString *attributedCaptionSummary;
@property (nonatomic, readwrite, nullable) NSAttributedString *attributedCaptionCredit;

@end

@implementation AMPhotoViewer

- (instancetype)initWithImage:(UIImage *)image {
    self = [super init];
    if (self) {
        _image = image;
    }
    
    return self;
}

@end
