//
//  AMPhotoPreviewCollectionViewController.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

// Frameworks
#import <NYTPhotoViewer/NYTPhotosViewController.h>

// Views
#import "AMPhotoCaption.h"
#import "AMPhotoPreviewViewModel.h"
#import "AMPhotoViewer.h"
#import "AMPreviewPhotoCell+ConfigureForPhoto.h"

// Models
#import "Photo.h"

#import "AMPhotoPreviewCollectionViewController.h"

#import "AMCollectionViewDataBinding.h"


static NSString *AMPreviewPhotoCellIdentifier = @"previewPhotoCell";

@interface AMPhotoPreviewCollectionViewController () <UICollectionViewDelegateFlowLayout, NYTPhotosViewControllerDelegate>

@property (strong, nonatomic) id<AMListViewModel> viewModel;
@property (strong, nonatomic) AMCollectionViewDataBinding *dataBinding;

@end

@implementation AMPhotoPreviewCollectionViewController

#pragma mark - Life Cycle

#pragma mark - Properties

- (id<AMListViewModel>)viewModel {
    if (!_viewModel) {
        _viewModel = [[AMPhotoPreviewViewModel alloc] initWithObjectID:self.categoryID dataBinding:self.dataBinding];
    }
    return _viewModel;
}

- (AMCollectionViewDataBinding *)dataBinding {
    if (!_dataBinding) {
        _dataBinding = [[AMCollectionViewDataBinding alloc] initWithCollectionView:self.collectionView];
    }
    return _dataBinding;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.viewModel numberOfSection];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.viewModel numberOfRowForSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AMPreviewPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:AMPreviewPhotoCellIdentifier forIndexPath:indexPath];
    [cell configureCellForPhoto:[self.viewModel objectAtIndexPath:indexPath]];
    
    return cell;
}

#pragma mark - UICollectionView DataDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    AMPreviewPhotoCell *cell = (AMPreviewPhotoCell*)[collectionView cellForItemAtIndexPath:indexPath];
    AMPhotoViewer *photoViewer = [[AMPhotoViewer alloc] initWithImage:cell.previewImageView.image];
    NYTPhotosViewController *photoViewController = [[NYTPhotosViewController alloc] initWithPhotos:@[photoViewer]];
    photoViewController.delegate = self;
    [self presentViewController:photoViewController animated:YES completion:nil];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect frame = self.collectionView.frame;
    return CGSizeMake(frame.size.width / 2 - 0.5, frame.size.width / 2);
}

#pragma mark - NYTPhotosViewControllerDelegate

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController referenceViewForPhoto:(id<NYTPhoto>)photo {
    NSIndexPath *indexPath = [self.collectionView indexPathsForSelectedItems].firstObject;
    AMPreviewPhotoCell *cell = (AMPreviewPhotoCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    return cell.previewImageView;
}

- (UIView * _Nullable)photosViewController:(NYTPhotosViewController *)photosViewController captionViewForPhoto:(id <NYTPhoto>)photo {
    AMPhotoCaption *captionView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AMPhotoCaption class]) owner:photosViewController options:nil].firstObject;
    NSIndexPath *indexPath = [self.collectionView indexPathsForSelectedItems].firstObject;
    Photo *photoObject = [self.viewModel objectAtIndexPath:indexPath];
    captionView.titleLabel.text = photoObject.title;
    captionView.authorLabel.text = photoObject.author;
    
    return captionView;
}

@end
