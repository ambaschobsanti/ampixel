//
//  AMPhotoPreviewViewModel.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "AMPhotoPreviewViewModel.h"

#import "AppDelegate.h"
#import "Category.h"
#import "Photo+Networking.h"

@interface AMPhotoPreviewViewModel () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSNumber *objectID;
@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) id<AMDataBinding> dataBinding;

@end

@implementation AMPhotoPreviewViewModel

- (instancetype)initWithObjectID:(NSNumber *)objectID dataBinding:(id<AMDataBinding>)dataBinding {
    self = [super init];
    if (self) {
        _objectID = objectID;
        _categoryName = [[Category allCategories] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"categoryID = %@", objectID]].firstObject.categoryName;
        _dataBinding = dataBinding;
        [Photo requestPhotoForCategoryName:_categoryName completionHandler:nil];
    }
    
    return self;
}

#pragma mark - Properties

- (NSFetchedResultsController *)fetchedResultsController {
    if (!_fetchedResultsController) {
         NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryID == %i", [self.objectID integerValue]];
        request.predicate = predicate;
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];
        request.sortDescriptors = @[sort];
        NSManagedObjectContext *managedObjectContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
        NSFetchedResultsController *fetchResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        fetchResultController.delegate = self;
        [fetchResultController performFetch:nil];
        
        _fetchedResultsController = fetchResultController;
    }
    
    return _fetchedResultsController;
}

#pragma mark - Interface

- (NSInteger)numberOfSection {
    return self.fetchedResultsController.sections.count;
}

- (NSInteger)numberOfRowForSection:(NSInteger)section {
    return self.fetchedResultsController.sections[section].numberOfObjects;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark - NSFetchedResultsControllerDelegate

//Should Implement all NSFetchedResultsControllerDelegate and make it animated when database change.
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
     [self.dataBinding reload];
}

@end
