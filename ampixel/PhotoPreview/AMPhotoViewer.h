//
//  AMPhotoViewer.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <NYTPhotoViewer/NYTPhoto.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface AMPhotoViewer : NSObject <NYTPhoto>


@property (nonatomic, readonly, nullable) UIImage *image;

@property (nonatomic, readonly, nullable) NSData *imageData;

@property (nonatomic, readonly, nullable) UIImage *placeholderImage;

@property (nonatomic, readonly, nullable) NSAttributedString *attributedCaptionTitle;

@property (nonatomic, readonly, nullable) NSAttributedString *attributedCaptionSummary;

@property (nonatomic, readonly, nullable) NSAttributedString *attributedCaptionCredit;

- (instancetype)initWithImage:(UIImage *)image;

@end
NS_ASSUME_NONNULL_END
