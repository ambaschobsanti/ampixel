//
//  AMPhotoPreviewViewModel.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AMListViewModel.h"

@interface AMPhotoPreviewViewModel : NSObject <AMListViewModel>

@end
