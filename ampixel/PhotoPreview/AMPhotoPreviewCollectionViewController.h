//
//  AMPhotoPreviewCollectionViewController.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMPhotoPreviewCollectionViewController : UICollectionViewController

@property (strong, nonatomic) NSNumber *categoryID;

@end
