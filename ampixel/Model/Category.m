//
//  Category.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "Category.h"

@implementation Category

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"categoryID": @"categoryID",
             @"categoryName": @"categoryName",
             };
}

+ (NSArray <Category *> *)allCategories {
    NSString *categoryJSONFilePath = [[NSBundle mainBundle] pathForResource:@"Category" ofType:@"json"];
    NSString *jsonString = [NSString stringWithContentsOfFile:categoryJSONFilePath encoding:NSUTF8StringEncoding error:nil];
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];

    NSMutableArray <Category *> *categories = [NSMutableArray array];
    for (NSDictionary *categoryDict in parsedData) {
        [categories addObject:[Category modelWithDictionary:categoryDict error:nil]];
    }
    return categories;
}

@end
