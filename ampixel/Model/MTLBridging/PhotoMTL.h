//
//  PhotoMTL.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <Mantle/Mantle.h>

#import <Foundation/Foundation.h>
#import <MTLManagedObjectAdapter/MTLManagedObjectAdapter.h>

@interface PhotoMTL : MTLModel <MTLJSONSerializing, MTLManagedObjectSerializing>

@property (assign, nonatomic) NSNumber *photoID;
@property (assign, nonatomic) NSNumber *categoryID;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *photoURLString;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSDate *createdAt;

@end
