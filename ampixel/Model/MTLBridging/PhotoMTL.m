//
//  PhotoMTL.m
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import "PhotoMTL.h"

@implementation PhotoMTL

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"photoID": @"id",
             @"categoryID": @"category",
             @"photoURLString": @"images",
             @"author": @"user.fullname",
             @"title": @"name",
             @"createdAt": @"created_at"
             };
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    return dateFormatter;
}

+ (NSValueTransformer *)photoURLStringJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray * images, BOOL *success, NSError *__autoreleasing *error) {
        return images.firstObject[@"url"];
    }];
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [[self dateFormatter] dateFromString:dateString];
    }];
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return [NSDictionary mtl_identityPropertyMapWithModel:self];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"photoID"];
}

+ (NSString *)managedObjectEntityName {
    return @"Photo";
}

@end
