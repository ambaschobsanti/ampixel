//
//  Photo+CoreDataProperties.m
//  
//
//  Created by Ambas Chobsanti on 6/19/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Photo+CoreDataProperties.h"

@implementation Photo (CoreDataProperties)

@dynamic photoID;
@dynamic categoryID;
@dynamic photoURLString;
@dynamic title;
@dynamic author;

@end
