//
//  Category.h
//  ampixel
//
//  Created by Ambas Chobsanti on 6/19/16.
//  Copyright © 2016 AM. All rights reserved.
//

#import <Mantle/Mantle.h>

#import <Foundation/Foundation.h>

@interface Category : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSNumber *categoryID;
@property (strong, nonatomic) NSString *categoryName;

+ (NSArray <Category *> *)allCategories; 

@end
