#  Ampixel

![](https://bytebucket.org/ambaschobsanti/ampixel/raw/d4d209d5466b10ea1f7bf637b77a6e7d68c24534/sample.gif?raw=true)

### What is this repository for? ###

* This Application will group photo from 500px in category.

### How do I get set up? ###

* use `pod install`
* open `ampixel.xcworkspace`

### Contribution guidelines ###

* This project conform with The New York Times Objective-C Style Guide